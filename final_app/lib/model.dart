import 'dart:convert';
class Corona {
  final String name, positif, sembuh, meninggal;
  Corona(this.name, this.positif, this.sembuh, this.meninggal);
}
CoronaModel coronaModelFromJson(String str) => CoronaModel.fromJson(json.decode(str));
String coronaModelToJson(CoronaModel data) => json.encode(data.toJson());
class CoronaModel {
  CoronaModel({
    this.name,
    this.positif,
    this.sembuh,
    this.meninggal,

  });
  String name;
  String positif;
  String sembuh;
  String meninggal;

  factory CoronaModel.fromJson(Map<String, dynamic> json) => CoronaModel(
    name: json["name"],
    positif: json["positif"],
    sembuh: json["sembuh"],
    meninggal: json["meninggal"],

  );
  Map<String, dynamic> toJson() => {
    "name": name,
    "positif": positif,
    "sembuh": sembuh,
    "meninggal": meninggal,

  };
}