import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class GetDataApi extends StatefulWidget {
  @override
  _GetDataApiState createState() => _GetDataApiState();
}

class _GetDataApiState extends State<GetDataApi> {
  get id => null;

  getCoronaData() async {
    var response = await http
        .get(Uri.parse("https://api.kawalcorona.com/indonesia"));
    var jsonData = jsonDecode(response.body);
    List<Corona> coronas = [];
    for (var u in jsonData) {
      Corona corona = Corona(u["name"], u["positif"], u["sembuh"], u["meninggal"]);
      coronas.add(corona);
    }
    print(coronas.length);
    return coronas;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Get Data"),
      ),
      body: Container(
        child: Card(
          child: FutureBuilder(
            future: getCoronaData(),
            builder: (context, snapshot){
              if (snapshot.data==null) {
                return Container(
                  child: Center(
                    child: Text("Loading....")
                  ),
                );
              }else{
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i){
                    return Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/img/cov.png"),
                          Text(
                            '${snapshot.data[i].name}\n',
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                            ),
                          Text(
                            'Jumlah Positif : ${snapshot.data[i].positif}\n'
                            'Jumlah Sembuh : ${snapshot.data[i].sembuh}\n'
                            'Jumlah Meninggal : ${snapshot.data[i].meninggal}\n',
                            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                        ],
                      ),
                    );
                    // return ListTile(
                    //   title: Text('${snapshot.data[i].name}'),
                    //   subtitle: Text(
                    //     'Jumlah Positif : ${snapshot.data[i].positif}\n'
                    //     'Jumlah Sembuh : ${snapshot.data[i].sembuh}\n'
                    //     'Jumlah Meninggal : ${snapshot.data[i].meninggal}\n'
                    //     ),
                    // );
                  }
                );
              }
            }
          ),
        ),
      ),
    );
  }
}
